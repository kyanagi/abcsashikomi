# -*- coding: utf-8 -*-
require 'erb'

class IllegalFlagValueException < Exception; end

def readNameList(file)
	lines = File.readlines(file)
	lines.map do |line|
		no, name, school, grade, rank = line.chomp.split(',')
		rank = rank.to_i
		if 1 <= rank && rank <= 49
			{"name" => name, "school" => school, "grade" => grade, "number" => no, "rank" => rank}
		end
	end.compact
end

f = open("template.tex", "r")
startOfTemplate = []
repeatingParts = []
endOfTemplate = []
flag = 1
f.each { |line|
	begin
		case flag
			when 1
				startOfTemplate.push(line)
			when 0
				repeatingParts.push(line)
			when -1
				endOfTemplate.push(line)
			else
				raise IllegalFlagValueException
			end
	rescue IllegalFlagValueException
		p "Illegal value is assigned to flag!"
	end
	
	if line =~ /begin_repeat/
		flag = 0
	elsif line =~ /end_repeat/
		flag = -1
	else
	end
}
f.close
beginPart = startOfTemplate.inject(""){|t,l|
	t.concat(l)
}
template = repeatingParts.inject(""){|t,l|
	t.concat(l)
}
endPart = endOfTemplate.inject(""){|t,l|
	t.concat(l)
}

list = readNameList(ARGV[0] || 'namelistsample.csv')
File.open("nameplate.tex", "w+"){|ftex|
	ftex.write(beginPart)
	erb = ERB.new(template)
	list.each{|ln|
		ftex.write(erb.result(binding))
	}
	ftex.write(endPart)
}
